var medGame = angular.module('medGameApp',
  [
    'ui.router',
    'medGameApp.myTowns',
    'medGameApp.town'
  ]);

medGame.run(['$rootScope', '$state', '$stateParams',
      function ($rootScope, $state, $stateParams) {
          // It's very handy to add references to $state and $stateParams to the $rootScope
          // so that you can access them from any scope within your applications.For example,
          // <li ng-class="{ active: $state.includes('contacts.list') }"> will set the <li>
          // to active whenever 'contacts.list' or one of its decendents is active.
          $rootScope.$state = $state;
          $rootScope.$stateParams = $stateParams;
      }]);

medGame.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    var appDirectory = "../Application/medgame_fe/app/";
    var mytownsDirectory = appDirectory + "my-towns";

    $stateProvider

        .state('root', {
            abstract: true,
            views: {

                '': {
                    templateUrl: appDirectory + '/views/layout.html'
                },

                'navigation@root': {
                    templateUrl: appDirectory + '/views/navigation.html'
                },

                'footer@root': {
                    templateUrl: appDirectory + '/views/footer.html'
                }

            }

        })

        .state('home', {
            url: '/',
            parent: 'root',
            templateUrl: appDirectory + 'test.html'
        })

        .state('my-towns', {
            url: '/my-towns',
            parent: 'root',
            templateUrl: mytownsDirectory + '/my-towns-list/views/content.html',
            controller: 'MyTownsListController'
        })

        .state('town', {
            url: '',
            parent: 'root',
            abstract: true,
            templateUrl: mytownsDirectory + '/town-detail/views/town.html',
            controller: 'TownDetailController'
        })

        .state('town.detail', {
            url: '/my-towns/:townId/detail',
            parent: 'town',
            templateUrl: mytownsDirectory + '/town-detail/views/town-detail.html',
            controller: 'TownDetailController'
        })

        .state('town.center', {
            url: '/my-towns/:townId/center',
            parent: 'town',
            templateUrl: mytownsDirectory + '/town-detail/views/town-center.html',
            controller: 'TownCenterController'
        })

        .state('town.local', {
            url: '/my-towns/:townId/local',
            parent: 'town',
            templateUrl: mytownsDirectory + '/town-detail/views/town-local.html',
            controller: 'TownLocalController'
        });



});
