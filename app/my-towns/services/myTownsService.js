angular.module('myTowns.services', [])
  .factory('Towns', function($resource) {
      return $resource('api/public/v1/myaccount/towns',
      {
        query:{
          method: 'GET'
        }
      });
  });
