function MyTownsController($scope, Towns) {
  $scope.towns = Towns.query();

};

myTownsModule.component('myTownsList', {
  templateUrl: '../Application/medgame_fe/app/my-towns/components/my-towns/my-towns.html',
  controller: MyTownsController,
  bindings: {
    hero: '='
  }
});
