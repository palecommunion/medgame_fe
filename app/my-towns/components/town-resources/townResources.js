var resources = {
  "TownId": 666,
  "Name": "Mestecko",
  "Budget": 3131,
  "Population": 25,
  "PopulationInc": 165,
  "Food": 564,
  "FoodInc": 456,
  "Morality": 555,
  "MoralityInc": 666,
};


function TownResourcesController() {
  this.resources = resources;

};

angular.module('medGameApp.myTowns')
    .component('townResources', {
      templateUrl: '../Application/medgame_fe/app/my-towns/components/town-resources/town-resources.html',
      controller: TownResourcesController,
      controllerAs: 'townResourcesCtrl'
    });
